#!/bin/bash
echo "Collecting data to create the database..."

read -p "Enter the database name: " db_name

read -p "Enter the database user: " db_user

while true; do
    read -s -p "Enter the database password: " db_pass
    echo
    read -s -p "Confirm the database password: " db_pass_confirm
    [ "$db_pass" = "$db_pass_confirm" ] && break
    echo -e "\nPasswords don't match. Please try again...\n"
done

file="./docker/env"

if [ -f "$file" ]
then
    rm "$file"
fi

printf "DB_NAME=$db_name\nDB_USER=$db_user\nDB_PASS=$db_pass\nDB_SERVICE=postgres\nDB_PORT=5432" >> "$file"

echo -e "\nDatabase configuration created succefully!"